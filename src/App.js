import React,{useEffect, useState} from 'react'
import { StylesProvider, createGenerateClassName } from '@material-ui/core/styles'
import axios from 'axios'
import SuratBaruAll from './components/SuratBaruAll'
const generateClassName = createGenerateClassName({
    productionPrefix: 'suratbaru-all',
  });
export default function App(props){
    return(
        <StylesProvider generateClassName={generateClassName}>
            <SuratBaruAll/>
         </StylesProvider>
    )
}