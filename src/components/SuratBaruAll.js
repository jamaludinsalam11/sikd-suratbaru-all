import React,{useEffect, useState} from 'react'
import {
    Grid,
    Fab, Chip, Button,
    Divider, Table,TableCell, TableBody, Paper,Typography,Link, TablePagination,
    CircularProgress,TableContainer, TableHead, TableRow, LinearProgress
} from '@material-ui/core'
import { withStyles,makeStyles } from '@material-ui/core/styles'
import Pagination from '@material-ui/lab/Pagination'


export default function SuratBaruAll(){
    const classes = useStyles()
    const [page, setPage] = useState(0)
    const [data, setData] = useState(null)
    const [loading, setLoading] = useState(true)
    const [notFound, setNotFound] = useState(false)
    const [totalPage, setTotalPage] = useState(null)
    const [row, setRow] = useState(5)
    const [rowsPerPage, setRowsPerPage] = useState(5)
    const [totalSurat, setTotalSurat] = useState(null)
    const [totalDisposisi, setTotalDisposisi] = useState(null)
    const [totalNotadinas, setTotalNotadinas] = useState(null)
    const [totalTembusan, setTotalTembusan] = useState(null)
    useEffect(() => {
        const timeout = setTimeout(async() => {
            try{
                /** For Development */
                // const rt = 'uk.1.1.18'
                // const ti = '576'
                /** For productions */
                const rt = window.rt
                const ti = window.ti

                const apiSuratBaru      = `${process.env.API_FASTIFY}/elasticsearch/suratbaru/all?rt=${rt}&ti=${ti}`
                const apiSuratUnread    = `${process.env.API_FASTIFY}/elasticsearch/notification/unread/core?rt=${rt}&ti=${ti}` 

                // const apiSuratBaru      = `http://localhost:3001/elasticsearch/suratbaru/all?rt=${rt}&ti=${ti}`
                // const apiSuratUnread    = `http://localhost:3001/elasticsearch/notification/unread/core?rt=${rt}&ti=${ti}` 

                // const [ fetchApiSuratBaru, fetchApiSuratUnread ] = await Promise.all([
                //     fetch(apiSuratBaru),
                //     fetch(apiSuratUnread)
                // ])
                const fetchApiSuratBaru = await fetch(apiSuratBaru)
                const fetchApiSuratUnread = await fetch(apiSuratUnread)

                const resApiSuratBaru   = await fetchApiSuratBaru.json()
                const resApiSuratUnread = await fetchApiSuratUnread.json()
                const { unread_total, unread_notadinas, unread_tembusan, unread_disposisi } = resApiSuratUnread.results
                setData(resApiSuratBaru.data)
                setLoading(false)
                setNotFound(resApiSuratBaru.data == null ? true :false)
                setTotalPage(resApiSuratBaru.totalPage)

                setTotalSurat(unread_total)
                setTotalDisposisi(unread_disposisi)
                setTotalNotadinas(unread_notadinas)
                setTotalTembusan(unread_tembusan)
                // console.log(resApiSuratBaru)
                // console.log(resApiSuratUnread)
                // console.log('rt', rt)
                // console.log('windows', window)
            } catch(err) {
                console.log('error:', err)
            }
        }, 30)
        
        return () => clearTimeout(timeout)
    },[page, rowsPerPage])

    const handlePage = (event, value) => {
        setPage(value);
    };
    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };
    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };
    return(
        <div className={classes.appContainerRiwayat} style={{justifyContent: 'center'}}>
            <Grid container spacing={2} style={{paddingBottom: '1em', paddingTop: '.8em'}}>
                <Grid item>
                    <Button variant="contained" color="primary" href="index3.php?option=MailInboxSuratBaruEs" size="small" style={{borderRadius: 25, color: '#fff', fontSize: '11px'}}>
                        {totalSurat&&totalSurat} Surat 
                    </Button>
                </Grid>
                <Grid item>
                    <Button variant="contained" color="primary" href="index3.php?option=MailInboxTTDBaruEs" disabled={true} size="small" style={{borderRadius: 25, color: '#fff', fontSize: '11px'}}>
                        0 Permohonan TTD
                    </Button>
                </Grid>
                <Grid item>
                    <Button variant="contained" color="primary" href="index3.php?option=MailInboxDisposisiBaruEs" disabled={false} size="small" style={{borderRadius: 25, color: '#fff', fontSize: '11px'}}>
                        {totalDisposisi&&totalDisposisi} Disposisi
                    </Button>
                </Grid>
                <Grid item>
                    <Button variant="contained" color="primary" href="index3.php?option=MailInboxNotadinasBaruEs" disabled={false} size="small" style={{borderRadius: 25, color: '#fff', fontSize: '11px'}}>
                        {totalNotadinas&&totalNotadinas} Nota Dinas
                    </Button>
                </Grid>
                <Grid item>
                    <Button variant="contained" color="primary" href="index3.php?option=MailInboxTembusanBaruEs" disabled={false} size="small" style={{borderRadius: 25, color: '#fff', fontSize: '11px'}}>
                        {totalTembusan&&totalTembusan} Tembusan
                    </Button>
                </Grid>
            </Grid>
            <TableContainer component={Paper} className={classes.containerTable}>
                {loading == true ? <LinearProgress style={{marginBottom: '-.4em'}} /> : '' }
                <Table className={classes.table} stickyHeader aria-label="sticky table">
                    {loading == true ? '' : notFound == true ? '' : 
                        <TableHead>
                            <TableRow>
                                <StyledTableCell align="left" width="12%" style={{width: '3%',fontSize: '12px'}}>No</StyledTableCell>
                                <StyledTableCell align="left" width="12%" style={{width: '5%',fontSize: '12px'}}>Status Surat</StyledTableCell>
                                <StyledTableCell align="left" width="12%" style={{width: '5%',fontSize: '12px'}}>Tgl Diterima</StyledTableCell>
                                <StyledTableCell align="left" width="20%" style={{width: '12%',fontSize: '12px'}}>Nomor Surat</StyledTableCell>
                                <StyledTableCell align="left" width="20%" style={{width: '20%',fontSize: '12px'}}>Asal Naskah</StyledTableCell>
                                <StyledTableCell align="left" width="20%" style={{width: '20%',fontSize: '12px'}}>Pengirim</StyledTableCell>
                                <StyledTableCell align="left" width="33%" style={{width: '33%',fontSize: '12px'}}>Perihal</StyledTableCell>
                                <StyledTableCell align="left" width="10%" style={{width: '10%',fontSize: '12px'}}>Jenis Surat</StyledTableCell>
                            </TableRow>
                        </TableHead>
                    }
                    
                    <TableBody>
                    {/* <LoadingList/> */}
                    {/* <NotFound/> */}
                    {loading == true ? <LoadingList/> : notFound == true ? <NotFound/> : data&&data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row, key) => (
                        <StyledTableRow key={key} hover={true} className={classes.tablerow} >
                            <StyledTableCell width="12%" className={row.statusreceive == 'unread' ? classes.unread : classes.read} align="left" style={{width: '3%'}}>
                                <Link href={`https://sikd_jamal.mkri.id/SIKD/index3.php?option=MailTL&id=${row.nid}`} className={row.statusreceive == 'unread' ? classes.unread : classes.read} color="inherit">{row.urutan}</Link>
                            </StyledTableCell>
                            <StyledTableCell width="10%"  className={row.statusreceive == 'unread' ? classes.unread : classes.read} align="left" style={{width: '5%'}}>
                                <Link href={`https://sikd_jamal.mkri.id/SIKD/index3.php?option=MailTL&id=${row.nid}`} className={row.statusreceive == 'unread' ? classes.unread : classes.read} color="inherit">{row.statusreceive}</Link>
                            </StyledTableCell>
                            <StyledTableCell width="10%"  className={row.statusreceive == 'unread' ? classes.unread : classes.read} align="left" style={{width: '5%'}}>
                                <Link href={`https://sikd_jamal.mkri.id/SIKD/index3.php?option=MailTL&id=${row.nid}`} className={row.statusreceive == 'unread' ? classes.unread : classes.read} color="inherit">{row.receivedate}</Link>
                            </StyledTableCell>
                            <StyledTableCell width="10%"  className={row.statusreceive == 'unread' ? classes.unread : classes.read} align="left" style={{width: '12%'}}>
                                <Link href={`https://sikd_jamal.mkri.id/SIKD/index3.php?option=MailTL&id=${row.nid}`} className={row.statusreceive == 'unread' ? classes.unread : classes.read} color="inherit">{row.nomor}</Link>
                            </StyledTableCell>
                            <StyledTableCell width="5%" className={row.statusreceive == 'unread' ? classes.unread : classes.read} align="left" style={{width: '5%'}}>
                                <Link href={`https://sikd_jamal.mkri.id/SIKD/index3.php?option=MailTL&id=${row.nid}`} className={row.statusreceive == 'unread' ? classes.unread : classes.read} color="inherit">{row.pengirim === 'external' ? row.instansipengirim : row.asal_naskah_jabatan } </Link>
                            </StyledTableCell>
                            <StyledTableCell width="5%" className={row.statusreceive == 'unread' ? classes.unread : classes.read} align="left" style={{width: '5%'}}>
                                <Link href={`https://sikd_jamal.mkri.id/SIKD/index3.php?option=MailTL&id=${row.nid}`} className={row.statusreceive == 'unread' ? classes.unread : classes.read} color="inherit">{row.pengirim === 'external' ? row.instansipengirim : row.jabatan_pengirim } </Link>
                            </StyledTableCell>
                            <StyledTableCell width="33%" className={row.statusreceive == 'unread' ? classes.unread : classes.read} align="left" style={{width: '33%'}}>
                                <Link href={`https://sikd_jamal.mkri.id/SIKD/index3.php?option=MailTL&id=${row.nid}`} className={row.statusreceive == 'unread' ? classes.unread : classes.read} color="inherit">{row.hal}</Link>
                            </StyledTableCell>
                            
                            <StyledTableCell width="7%" className={row.statusreceive == 'unread' ? classes.unread : classes.read} align="left" style={{width: '8%'}}>
                                <Link href={`https://sikd_jamal.mkri.id/SIKD/index3.php?option=MailTL&id=${row.nid}`} className={row.statusreceive == 'unread' ? classes.unread : classes.read} color="inherit">
                                    <Chip label={row.jenis} className={row.jenis === 'Disposisi' ? classes.disposisi : row.jenis === 'Notadinas' ? classes.notadinas : classes.tembusan}/>
                                </Link>
                            </StyledTableCell>
                            
                        </StyledTableRow>
                        
                    ))}
                
                    </TableBody>
                </Table>
            </TableContainer>
            {notFound == true 
                ? ''
                :   <TablePagination
                        rowsPerPageOptions={[ 5, 10, 20]}
                        component="div"
                        count={data&&data.length}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        onPageChange={handleChangePage}
                        onRowsPerPageChange={handleChangeRowsPerPage}
                    />
            
            }
            
            
        </div>
    )
}
// {notFound || loading == true 
//     ? '' 
//     : 
//     <div className={classes.pagination}>
//         <Grid container direction="row"  alignItems="center">
//             <Grid item>
//                 {/* <Pagination count={30} color="primary" /> */}
//                 <Pagination count={totalPage > 1 ? totalPage : 1 } page={totalPage > 1 ? page : 1} onChange={handlePage} color="primary"/>
//             </Grid>
//         </Grid>
//     </div>
// }
// const useStyles = createUseStyles((theme) => ({
//     appContainerRiwayat: {
//         width: '100%',
//         height: '600px',
//         // backgroundColor: '#E2E8EE'
//     },
//     disposisi: {
//         backgroundColor: 'rgb(220, 0, 78)',
//         color: '#fff'
//     },
//     notadinas: {
//         backgroundColor: '#1976d2',
//         color: '#fff'
//     },
//     tembusan: {
//         backgroundColor: 'rgb(241, 225, 91)',
//         color: 'rgba(0, 0, 0, 0.9)'
//     },
//     pagination: {
//         paddingTop: theme.spacing(5),
//         textAlign: 'center',
//         justifyContent: 'center',
//         display: 'grid'
//     },
//     read: {
//         fontSize: 12,
    
//     },
//     unread: {
//         fontSize: 12,
//         fontWeight: 'bold'
//     },
// }));
const useStyles = makeStyles((theme) => ({
    appContainerRiwayat: {
        width: '100%',
        height: 'auto',
        // backgroundColor: '#E2E8EE'
    },
    containerTable: {
        container: {
            height: 1040,
        }
    },
    disposisi: {
        backgroundColor: 'rgb(220, 0, 78)',
        color: '#fff'
    },
    notadinas: {
        backgroundColor: '#1976d2',
        color: '#fff'
    },
    tembusan: {
        backgroundColor: 'rgb(241, 225, 91)',
        color: 'rgba(0, 0, 0, 0.9)'
    },
    pagination: {
        paddingTop: theme.spacing(5),
        textAlign: 'center',
        justifyContent: 'center',
        display: 'grid'
    },
    read: {
        fontSize: 12,
    
    },
    unread: {
        fontSize: 12,
        fontWeight: 'bold'
    },
}));

const StyledTableCell = withStyles((theme) => ({
    head: {
      // backgroundColor: theme.palette.common.black,
      backgroundColor: '#F9FAFB',
      // color: theme.palette.common.white,
      color: 'black',
      borderTopColor: '#3F51B5',
      borderTopWidth: '4px',
      borderTopStyle: 'solid'
    },
    body: {
      fontSize: 14,
    }
  }))(TableCell);
  
const StyledTableRow = withStyles((theme) => ({
    root: {
      '&:nth-of-type(odd)': {
      //   backgroundColor: theme.palette.action.hover,
        backgroundColor: '#D6E4F2',
      },
    },
    hover: {
      backgroundColor: ''
    }
}))(TableRow);

const LoadingList = () => {
    return (
        <StyledTableRow style={{width: 1200, backgroundColor: '#DEE3F1', height: 500, opacity:.5}}>
            <StyledTableCell  colSpan='8' align="center" style={{width: '700px'}}>
                <Typography variant="h1">
                    <CircularProgress />
                    {/* <img src={searchgif} style={{height: 'auto', width: '60%'}}/> */}
                    {/* <Typography>Searching data what you want </Typography> */}
                </Typography>
            </StyledTableCell>
        </StyledTableRow>
    )
}

const NotFound = () => {
    return (
        // <StyledTableRow style={{width: 1200, backgroundColor: '#F0F4F8', height: 500}}>
        <StyledTableRow style={{width: 1200, backgroundColor: '#fff', height: 500}}>
            <StyledTableCell  colSpan='8' align="center" style={{width: '700px'}}>
                <Typography variant="h1">
                    {/* <img src={notfound2} style={{height: 'auto', width: '30%'}}/> */}
                </Typography>
                <Typography variant="subtitle1" style={{color: '#617E96'}}> Tidak ada surat baru</Typography>
            </StyledTableCell>
        </StyledTableRow>
    )
}